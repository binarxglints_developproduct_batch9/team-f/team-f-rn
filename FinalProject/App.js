/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState, useEffect, useDebugValue} from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { SafeAreaProvider} from 'react-native-safe-area-context';
import {Provider, useSelector, useDispatch} from 'react-redux'

//Action
import {checkStorage} from './src/store/action'

//SplashPage
import SplashScreen from './src/screens/SplashScreen';

//AuthPage
import LoginPage from './src/screens/LoginPage'
import SignUpPage from './src/screens/SignUpPage'
import RolePage from './src/screens/RolePage'

//TeacherPage
import TeacherNavigation from './src/navigations/TeacherNavigation'

//StudentPage
import StudentNavigation from './src/navigations/StudentNavigation'

//Store Reducer and Saga
import store from './src/store'


const Stack = createStackNavigator();

function AuthScreen () {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Role" component={RolePage} options={{title: "LEKTUR", headerTitleAlign:"center"}}/>
      <Stack.Screen name="LogIn" component={LoginPage} options={{headerTitleAlign:"center"}}/>
      <Stack.Screen name="SignUp" component={SignUpPage} options={{headerTitleAlign:"center"}}/>
    </Stack.Navigator>
  )
}

const App: () => React$Node = () => {
  const {token, role} = useSelector(state=>state.user)
  const [hideSplash, setHideSplash] = useState(false);
  const dispatch = useDispatch()
  useEffect(()=>{
    dispatch(checkStorage())
    setTimeout(()=>{
      setHideSplash(true)
    }, 1000)
  },[])
  return (
    <SafeAreaProvider>
      <NavigationContainer>
        <Stack.Navigator headerMode="none">
          {hideSplash == false &&
            <Stack.Screen name="SplashScreen" component={SplashScreen}/>
          }
          {token === '' && 
            <Stack.Screen name="AuthScreen" component={AuthScreen} />
          }
          { token!='' && role == 'student' &&
            <Stack.Screen name="StudentNavigation" component={StudentNavigation}/>
          }
          { token!='' && role == 'teacher' &&
            <Stack.Screen name="TeacherNavigation" component={TeacherNavigation}/>
          }
        </Stack.Navigator>
      </NavigationContainer>
    </SafeAreaProvider>
  );
};

export default () => {
  return(
  <Provider store={store}>
    <App/>
  </Provider>
  )
};
