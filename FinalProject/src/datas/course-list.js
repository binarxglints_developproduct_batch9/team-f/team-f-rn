import CourseList from '../models/course-list'

const CourseListed = [
  new CourseList(
    //id, imageUrl, totalStudent, title, teacher, totalVideos,totalLesson, description, category
    1,
    'https://cdn.pixabay.com/photo/2016/10/02/22/17/red-t-shirt-1710578_1280.jpg',
    156,
    'Sales and Marketing',
    'Susan Sarah',
    15,
    3,
    'Aliqua commodo nulla enim et amet irure irure mollit ad do. Aliqua minim dolore irure occaecat deserunt. Incididunt elit reprehenderit laborum enim quis commodo qui consequat ullamco ut aliquip nulla. Ut elit irure nulla occaecat dolore sit et labore duis sit. Cillum aliqua ullamco voluptate deserunt aliqua.',
    'Marketing'
  ),
  new CourseList(
    2,
    'https://images.pexels.com/photos/6292/blue-pattern-texture-macro.jpg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    120,
    'Create Cinematic Music Video',
    'John Doe',
    12,
    4,
    'Aliqua commodo nulla enim et amet irure irure mollit ad do. Aliqua minim dolore irure occaecat deserunt. Incididunt elit reprehenderit laborum enim quis commodo qui consequat ullamco ut aliquip nulla. Ut elit irure nulla occaecat dolore sit et labore duis sit. Cillum aliqua ullamco voluptate deserunt aliqua.',
    'Art & Humanity'
  ),
  new CourseList(
    3,
    'https://cdn.pixabay.com/photo/2016/10/02/22/17/red-t-shirt-1710578_1280.jpg',
    99,
    'Public Speaking Like a Pro',
    'Colt Steele',
    17,
    6,
    'Aliqua commodo nulla enim et amet irure irure mollit ad do. Aliqua minim dolore irure occaecat deserunt. Incididunt elit reprehenderit laborum enim quis commodo qui consequat ullamco ut aliquip nulla. Ut elit irure nulla occaecat dolore sit et labore duis sit. Cillum aliqua ullamco voluptate deserunt aliqua.',
    'Business'
  ),
  new CourseList(
    4,
    'https://images.pexels.com/photos/6292/blue-pattern-texture-macro.jpg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    100,
    'History of Lektur',
    'Gery',
    12,
    4,
    'Aliqua commodo nulla enim et amet irure irure mollit ad do. Aliqua minim dolore irure occaecat deserunt. Incididunt elit reprehenderit laborum enim quis commodo qui consequat ullamco ut aliquip nulla. Ut elit irure nulla occaecat dolore sit et labore duis sit. Cillum aliqua ullamco voluptate deserunt aliqua.',
    'History'
  )
]
export default CourseListed