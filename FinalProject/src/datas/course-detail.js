import CourseDetails from '../models/course-details'

const CourseDetailed = new CourseDetails(
  'https://cdn.pixabay.com/photo/2016/10/02/22/17/red-t-shirt-1710578_1280.jpg',
  "Create Cinematic Music Video",
  "Justin Junaedi",
  'Cinematography',
  14,
  12,
  'Ullamco esse aliquip nostrud in dolore sint Lorem anim ut tempor sint enim id ea. Amet ex do in pariatur eiusmod. Ad irure ipsum officia veniam sint ut do. Nulla veniam duis fugiat mollit consectetur enim aliquip sunt. Aliquip magna tempor ut ut laborum fugiat. Commodo enim nisi duis dolor aute tempor ut mollit ea amet esse quis excepteur laboris. Officia tempor veniam id ullamco in tempor commodo excepteur laborum.',
  [
    {id:'1', status: 'play', title: 'Lesson #1: What is React?'},
    {id:'2', status: 'play', title: 'Lesson #2: Setup Environment'},
    {id:'3', status: 'play', title: 'Lesson #3: Create React App'},
    {id:'4', status: 'restrict', title: 'Lesson #4: CSS in React'},
    {id:'5', status: 'restrict', title: 'Lesson #5: Axios in React'},
    {id:'6', status: 'restrict', title: 'Lesson #6: Drag and Drop in React'},
    {id:'7', status: 'restrict', title: 'Lesson #7: Lorem Ipsim'},
    {id:'8', status: 'restrict', title: 'Lesson #8: Lorem Ipsim'},

  ]
)

export default CourseDetailed