import React, {useState} from 'react';
import { Text, View, TextInput, Dimensions, FlatList, TouchableOpacity, Touchable } from 'react-native';

import STYLES from '../../styles/style'

const width = Math.round(Dimensions.get("screen").width)
const height = Dimensions.get("screen").height

const AddCoursePage = ({
  dataHeader, onChange
}) => {
	const [editable, setEditable] = useState(true)
  return (
		<>
		
    <View style={STYLES.headerCourseForm}>
		{editable &&
		<>
     	<FlatList 
				data={dataHeader}
				keyExtractor={(item,index)=>index.toString()}
				renderItem={({item, index})=> {
					return(
					<>
						<TextInput
							autoCapitalize='words'
							autoCorrect={false} 
							style={STYLES.titleForm}
							value={item.title}
							placeholder='Title*'
							onChangeText={(value)=>onChange(index,'title', value)}
						/>
						<TextInput 
							autoCapitalize='sentences'
							autoCorrect={false} 
							textAlignVertical='top'
							multiline
							numberOfLines={10}
							style={STYLES.overviewForm}
							value={item.overview}
							placeholder='Overview*'
							onChangeText={(value)=>onChange(index,'overview', value)}
						/>
					</>
					)
				}
			}
			/>
			<View style={{marginTop:10, width: width}}>
				<TouchableOpacity style={{...STYLES.buttonWhite, width: "50%"}}>
					<Text style={{...STYLES.textOrange, fontWeight:'bold'}}>Add Image Header</Text>
				</TouchableOpacity>
				<Text style={STYLES.subText}>Max. size 5 MB. Supported format .png/jpg/jpeg</Text>
			</View>
			<View style={{alignItems:'flex-end'}}>
				<TouchableOpacity 
					style={{...STYLES.buttonForm, width: width*0.25}}
					onPress={()=>setEditable(false)}
				>
					<Text style={STYLES.textForm}>Save</Text>
				</TouchableOpacity>
			</View>
			</>}
			{!editable &&
			<>
				
			</>
			}
		</View>
		<View style={STYLES.bodyCourseForm}>

		</View>
		</>
)};

export default AddCoursePage;
