import React, {useEffect} from 'react';
import { Text, FlatList, TouchableOpacity, Dimensions } from 'react-native';
import { SafeAreaView} from 'react-native-safe-area-context';
import {useSelector, useDispatch} from 'react-redux'

import OwnListComponent from '../../components/ownList'
import {getUser, getCourseByTeacher, deleteListStudent} from '../../store/action'
import VirtualizedView from '../../components/VirtulalizedView'

const width = Math.round(Dimensions.get("screen").width)
const height = Dimensions.get("screen").height

const DashBoardPage = ({
    navigation,
}) => {
	const ownList = useSelector(state=>state.courseTeacher)
	const user = useSelector(state=>state.user)
	const dispatch = useDispatch()
	useEffect(()=>{
		dispatch(getUser(user.token))
	},[])

	useEffect(() => {
		const listener = navigation.addListener('focus', () => {
			dispatch(getCourseByTeacher(user.token))
			dispatch(deleteListStudent())
    });
    return listener
	}, [navigation]);

	if(ownList == []){
		return null
	}

  return(
  <SafeAreaView>
		{/* <TouchableOpacity 
			style={{margin: width*0.02, backgroundColor: "#16213B", height: height*0.045, justifyContent:"center", borderRadius: 12}}
			onPress={()=>{navigation.navigate('NewCoursePage')}}	
		>
			<Text style={{alignSelf:"center", color:"#fff"}}>Add New Course</Text>
		</TouchableOpacity> */}
		<VirtualizedView>
		<FlatList 
			data={ownList}
			renderItem={({item}) => {
				return(
					<OwnListComponent 
						id = {item.id}
						imageUrl = {item.image}
						titleCourse = {item.title}
						totalVideos = {item.lesson}
						totalLesson = {item.material}
						totalStudent = {item.enrolled}
					/>
				)
			}}
		/>
		</VirtualizedView>
  </SafeAreaView>
)};

export default DashBoardPage;
