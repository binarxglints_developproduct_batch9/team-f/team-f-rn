import React, {useState, useEffect} from 'react';
import {
  View,TextInput,
} from 'react-native';
import { SafeAreaView} from 'react-native-safe-area-context';
import {useSelector, useDispatch} from 'react-redux'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import VirtualizedView from '../../components/VirtulalizedView'
import ListStudent from '../../components/ListStudent'
import STYLES from '../../styles/style'

const ListStudentPage = ({navigation}) => {
  const [search, setSearch] = useState('') 
  // Untuk kotak penulisan, saya cek kalau dia kosong (''), maka tampilin semua yang di listStudent
  //kalau berisi, saya tampilin filter
  const listStudent = useSelector(state => state.listStudent)
  const [showList, setShowList] = useState([])

  const activeStudent = listStudent.filter(student => {
    return student.status === 'active'
  })
  const completedStudent = listStudent.filter(student => {
    return student.status === 'completed'
  })
  const pendingStudent = listStudent.filter(student => {
    return student.status === 'pending'
  })
  const result = [...activeStudent, ...completedStudent, ...pendingStudent]

  // 1. Use Effevct dispatch agar dpt list rumah sakit
  // 2. listRS -> useSelector -> langsung .map || Problemnya kalau mau filter berarti saya harus ubah state di reducer
  // 3. Saya bikin variable baru yang untuk menampung data secara dinamis -> showList

  useEffect(()=>{
    if(search.trim() == ''){
      setShowList(result)
    } else {
      setShowList(result.filter(student => {
        console.log(search)
        return student.user.name.toLowerCase().includes(search.toLowerCase())
      }))
      //Hasilnya array
    }
  },[search])

  useEffect(()=>{
    setShowList(result)
  },[listStudent])
  if (listStudent.length == 0) {
    return null
  }
  return(
    <SafeAreaView style={{padding: 10}}>
      <View style={{alignItems:'center'}}>
        <TextInput style={STYLES.inputForm} 
          placeholder="Search"
          placeholderTextColor='#696969'
          value={search}
          onChangeText={(value)=>{setSearch(value)}}
        />
        <MaterialIcons name="search" style={{position:'absolute', fontSize: 24, top: 10, right: 30}}/>
      </View>
      <VirtualizedView>
      <View style={{marginBottom: 100}}>
          <ListStudent 
            data={showList}
          />
          
      </View>
      </VirtualizedView>
    </SafeAreaView>
  )
}

export default ListStudentPage;
