import React from 'react';
import {
  View,
  Text,
  TextInput,
  Image,
  TouchableOpacity
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';

import {signOut} from '../../store/action'
const TeacherProfilePage = ({
    navigation,
}) => {
  const user = useSelector(state => state.user)
  const dispatch = useDispatch()
  return(
  <View style={{
      flex:1,
			backgroundColor:'#ffffff',
			shadowOpacity: 0.26,
			shadowOffset: {width: 0, height: 2},
			shadowRadius: 8,
			elevation: 5,
      alignItems:'center',
			justifyContent:'center',
			marginHorizontal: 50,
			marginVertical: 80
		}}
		>
      <Image style={{
        width:175,
        height:175,
        borderRadius: 100,
        marginBottom:5}}
        source={{uri: user.image == 'http://ec2-3-0-16-51.ap-southeast-1.compute.amazonaws.com:3000/imageUser/null' ? 'https://png.pngtree.com/png-vector/20191009/ourmid/pngtree-user-icon-png-image_1796659.jpg' : `${user.image}`}}/>
      <Text
        style={{
         fontSize: 16,
         fontWeight: 'bold',
         color: '#16213B'
         }}>
        {user.name}
      </Text>
      <Text
        style={{
         fontSize: 16,
         color: '#16213B'
        }}>
        {user.email}
      </Text>
   
      <TouchableOpacity style={{
        width:150, 
        height:50, 
        backgroundColor:'#16213B',
        alignItems:'center',
        justifyContent:'center',
        borderRadius:0,
        marginTop:35,
      }}
        onPress={() => {navigation.navigate("EditProfile")}}>
        <Text style={{
          color:'#ffffff',
          fontSize:15}}>
          Edit Profile
        </Text>
      </TouchableOpacity>  
      <TouchableOpacity style={{marginTop: 40}}
        onPress={
          async () => {
            await AsyncStorage.removeItem('token')
            await AsyncStorage.removeItem('role')
            dispatch(signOut())
        }}
      >
        <Text style={{
          color:'#3E89AE', 
          fontSize:15}}
        >
          Sign Out
        </Text>
      </TouchableOpacity>     
    </View>
)};

export default TeacherProfilePage;
