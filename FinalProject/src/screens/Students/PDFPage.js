import React from 'react';
import { Text, View, Dimensions, TouchableOpacity } from 'react-native';
import Pdf from 'react-native-pdf';

import STYLES from '../../styles/style'

const width = Math.round(Dimensions.get("screen").width)
const height = Dimensions.get("screen").height

const PDFPage = ({
  route,
}) => {
  const {linkPdf} = route.params
  console.log(linkPdf)
  return (
  <View style={{flex:1}}>
    <Pdf
			source={{uri: `https://lektur.kuyrek.com/lessonAssets/${linkPdf}`}}
			style={{
				flex:1,
				width:width,
				height:height
			}}
		/>
  </View>
)};

export default PDFPage;
