import React, {useEffect} from 'react';
import { Text, View } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import {useSelector, useDispatch} from 'react-redux';

import {getEnrolledCourse, deleteLessonCourse} from '../../store/action'
import VirtualizedView from '../../components/VirtulalizedView'
import EnrolledCourse from '../../components/EnrolledCourse'

const CourseListPage = ({
    navigation,
}) => {
  const token = useSelector(state => state.user.token)
  const listCourse = useSelector(state => state.enrolledCourse)
  const dispatch = useDispatch()
  useEffect(() => {
    const listener = navigation.addListener('focus', () => {
      dispatch(getEnrolledCourse({token}))
      dispatch(deleteLessonCourse())
    });
    return listener    
  }, []);
  const activeCourse = listCourse.filter(course => {
    return course.status === 'active'
  })
  const pendingCourse = listCourse.filter(course => {
    return course.status === 'pending'
  })
  const completeCourse = listCourse.filter(course => {
    return course.status === 'completed'
  })
  const result = [...activeCourse, ...pendingCourse,...completeCourse]
  console.log(result)
  return (
    <SafeAreaView style={{flex:1}}>
    <VirtualizedView>
      <EnrolledCourse 
        data = {result}
      />
      </VirtualizedView>
    </SafeAreaView>
)};

export default CourseListPage;
