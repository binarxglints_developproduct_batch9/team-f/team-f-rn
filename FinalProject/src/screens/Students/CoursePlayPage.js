import React, { useState, useEffect } from 'react';
import { Text, View, SafeAreaView, Dimensions, FlatList, TouchableOpacity } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
// import Video from 'react-native-video';
import Video from 'react-native-clwy-video-player'
import {useSelector, useDispatch} from 'react-redux';

import VideoComponent from '../../components/VideoComponent'
import VirtualizedView from '../../components/VirtulalizedView'
import STYLES from '../../styles/style'
import authAPI from '../../api/auth'
import {getLessonsCourse} from '../../store/action'

const width = Math.round(Dimensions.get("screen").width)
const height = Dimensions.get("screen").height

const nextLesson = async(token, id) => {
	try{
		const res = await authAPI.patch(`/studentLessons/completed/${id}`,{},{'headers': { 'Authorization': `Bearer ${token}`}})
	} catch(e){
		console.log(e)
	}
}

const CoursePlayPage = ({
  route, navigation,
}) => {
	const {id, title, poster} = route.params
	const lessons = useSelector(state=>state.lesson)
	const token = useSelector(state => state.user.token)
	const [index, setIndex] = useState(route.params.index)
	const [seeMore, setSeeMore]=useState(true)
	const [isTruncatedText, setIsTruncatedText] = useState(false)
	const dispatch	= useDispatch()
	console.log(lessons)
	
	const [fullscreen, setFullscreen] = React.useState(false)
  useEffect(() => {
    navigation.setOptions({ headerShown: !fullscreen })
	}, [fullscreen, navigation])
	const onFullScreen = (status) => {
    setFullscreen(status);
	};
	if (lessons.length == 0){
		return null
	}
	console.log(fullscreen)
  return (
    <SafeAreaView style={{flex:1}}>
			{!fullscreen && <View style={{padding: 20}}>
				<View style={{flexDirection:'row'}}>
					<Text style={{fontSize:11, color:'#999999', textDecorationLine:'underline'}}>{title}/</Text>
					<Text style={{fontSize:11, color:'#3E89AE'}}>{lessons[index].lesson.title}</Text>
				</View>
				<Text style={{fontSize: 18, fontWeight:'bold'}}>{title}</Text>
			</View>}
			<View style={{justifyContent: 'center'}}>
				{/* <Video
					poster={poster}
					posterResizeMode='stretch'
					controls
					paused
					source={{uri: `https://lektur.kuyrek.com/lessonAssets/${lessons[index].lesson.video}`}}
					style={{flex:1}}  
					resizeMode="stretch"
				/> borderWidth:1, height:275*/}
				<Video
          url={`https://lektur.kuyrek.com/lessonAssets/${lessons[index].lesson.video}`}
          autoPlay
          logo={poster}
          placeholder={poster}
          hideFullScreenControl={false}
          onFullScreen={status => onFullScreen(status)}
          rotateToFullScreen
        />
			</View>
			{!fullscreen && <VirtualizedView>
			<View style={{...STYLES.shadow, padding: 20, marginHorizontal:20, marginTop: height*0.05, backgroundColor:'#fff', borderWidth: 1, borderColor: '#E5E5E5'}}>
				{isTruncatedText ?
					<>
						<Text numberOfLines={seeMore ? 5 : 0}>{lessons[index].lesson.description}</Text>
						<Text onPress={()=>setSeeMore(!seeMore)} style={{fontWeight:'bold'}}>
						{seeMore ? 'Read More' : 'Less'}
						</Text>
					</> :
					<Text 
						numberOfLines={5}
						onTextLayout={(event)=>{
							const {lines} = event.nativeEvent
							setIsTruncatedText(lines?.length > 5)
						}}
					>
						{lessons[index].lesson.description}
					</Text>
				}
			</View>

			<View style={{marginHorizontal: height*0.03, padding: 15}}>
				<Text style={{fontSize: 18, fontWeight:'bold'}}>What's Next</Text>
				<FlatList 
					data={lessons[index].lesson.material}
					keyExtractor={item => item}
					renderItem={({item, index}) => {
						return (
							<View>
							<TouchableOpacity 
								style={{flexDirection:'row', alignItems: 'center', margin: 5}}
								onPress={()=>navigation.navigate("PDF", {linkPdf: item})}
							>
								<MaterialIcons name="panorama-fish-eye" size={12} />
								<Text style={{color:'#3E89AE', textDecorationLine:'underline', fontSize: 18, marginLeft: 5}}>Material {index + 1}</Text>
							</TouchableOpacity>
							</View>
						)
					}}
				/>
			</View>
		<View>
				{lessons[index + 1] && 
				<TouchableOpacity style={{...STYLES.buttonBlue, padding:25}}
					onPress={async ()=>{
						console.log(lessons[index])
						if(lessons[index + 1].isPermission == false || lessons[index].isCompleted == false){
							await nextLesson(token, lessons[index]._id)
							await dispatch(getLessonsCourse({id: id, token: token}))
						}
						await setIndex(index + 1)
					}}
				>
					<MaterialIcons name="play-circle-fill" size={24} style={{color:'#fff'}}/>
					<Text style={{...STYLES.textWhite, fontSize: 14, fontWeight: 'bold'}}>Next: {lessons[index + 1].lesson.title}</Text>
				</TouchableOpacity>}
				{lessons[index].isCompleted == false && index == lessons.length - 1 && 
				<TouchableOpacity style={STYLES.buttonBlue}
					onPress={async ()=>{
						if(lessons[index].isCompleted == false){
							await nextLesson(token, lessons[index]._id)
							await dispatch(getLessonsCourse({id: id, token: token}))
						}
					}}
				>
					<MaterialIcons name="play-circle-fill" size={24} style={{color:'#fff'}}/>
					<Text style={{...STYLES.textWhite, fontSize: 14, fontWeight: 'bold'}}>Finished</Text>
				</TouchableOpacity>}
				<Text style={{fontSize: height * 0.03, fontWeight:'bold', marginBottom: height * 0.01}}>Content</Text>
			<VideoComponent 
					data={lessons}
					onClicked={(value)=>setIndex(value)}
					target={index}
				/>	
			</View>
			</VirtualizedView>}
    </SafeAreaView>
)};

export default CoursePlayPage;
