import React, {useState} from 'react';
import { ActivityIndicator, View, Modal, Dimensions } from 'react-native';

const width = Math.round(Dimensions.get("screen").width)
const height = Dimensions.get("screen").height

const ActivityIndicatorModal = ({visible}) => {
  return (
    <Modal
			animationType="slide"
			visible={visible}  
			transparent
		>
			<View style={{ backgroundColor: 'rgba(229,229,229,0.5)', flex:1}}>
				<View style={{backgroundColor:'#fff', marginTop:height *0.35, marginHorizontal:width* 0.4, borderRadius: 12, overflow:'hidden', padding:10, justifyContent:'center'}}>
          <ActivityIndicator size="large" color="#EF9C27"/>
			  </View>
			</View>
		</Modal>
)};

export default ActivityIndicatorModal;
