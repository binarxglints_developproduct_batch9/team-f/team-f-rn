import React, {useState} from 'react';
import { Text, View, Modal, Image, Dimensions, TouchableOpacity } from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import {clearEnroll, getEnrolledCourse} from '../../store/action'
import STYLES from '../../styles/style';

const width = Math.round(Dimensions.get("screen").width)
const height = Dimensions.get("screen").height

const EnrollSuccessModal = () => {
	const dispatch = useDispatch()
	const token = useSelector(state => state.user.token)
	const modalDetail = useSelector(state => state.modal)
	if(modalDetail.visible == null){
		return null
	}
  return (
    <Modal
			animationType="slide"
			visible={modalDetail.visible}  
			onRequestClose={()=>{
				dispatch(clearEnroll())
			}}
			onShow={()=>{
				dispatch(getEnrolledCourse({token}))
			}}
			transparent
		>
			<View style={{ backgroundColor: 'rgba(229,229,229,0.5)'}}>
				<View style={{backgroundColor:'#fff', marginTop:height *0.2, height: height * 0.4, marginHorizontal:width* 0.1, borderRadius: 12, overflow:'hidden', padding:10}}>
					<TouchableOpacity style={{alignSelf:'flex-end'}} onPress={()=>{dispatch(clearEnroll())}}>
						<MaterialIcons name="cancel" size={30}/>
					</TouchableOpacity>
					<Text style={{...STYLES.active, alignSelf:'center', fontSize: 20}}>{modalDetail.status}!</Text>
					<Image 
						style={{width: "100%", height: height*0.2, marginTop: height*0.02}}
						source={{uri:`${modalDetail.image}`}}
					/>
					<Text style={STYLES.nameList}>{modalDetail.title}</Text>
					<Text style={{...STYLES.status, fontSize: 14}}>By {modalDetail.teacher}</Text>
					<Text style={{marginTop: 20, alignSelf:'center'}}>Please wait coresponding teacher approve you!</Text>
			</View>
			</View>
		</Modal>
)};

export default EnrollSuccessModal;
