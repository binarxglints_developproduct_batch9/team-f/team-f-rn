import React, {useEffect, useState} from 'react';
import { Text, View, Dimensions, FlatList, TouchableOpacity, Alert, SectionList } from 'react-native';
import {useSelector, useDispatch} from 'react-redux';

import authAPI from '../../api/auth'
import OptionsCard from '../../components/OptionsCard';
import STYLES from '../../styles/style';
import ActivityIndicatorModal from './ActivityIndicatorModal';

const width = Math.round(Dimensions.get("screen").width)
const height = Dimensions.get("screen").height

const TestPage = ({
  route, navigation
}) => {
	const {test_id, title, student_test_id} = route.params
	const token = useSelector(state => state.user.token)
	const [question, setQuestion] = useState([])
	const [answer, setAnswer] = useState([])
	const [visible, setVisible] = useState(true)
	const getAnswer = (index, value) =>{
		let temp = answer
		temp[index] = value
		setAnswer(temp)
	}
	useEffect(() => {
		async function getQuestion(){
			try{
				const response = await authAPI.get(`/questions/byTest/${test_id}`, {'headers': { 'Authorization': `Bearer ${token}`}})
				setQuestion(response.data.data)
				for(let i = 0; i < response.data.data.length; i++){
					getAnswer(i, "")
				}
				setVisible(false)
			} catch(e){
				console.log(e)
			}
		}
		getQuestion()
	}, []);
	
	const uploadAnswer = async () => {
		console.log(question.length)
		console.log(answer.length)
		for(let i = 0; i < question.length; i++){
			try{
				const response = await authAPI.post(`/studentTests/create/answer`,{"student_test_id": `${student_test_id}`, "question_id": `${question[i]._id}`, "option_id": `${answer[i]}`}, {'headers': { 'Authorization': `Bearer ${token}`}})
				console.log(response)
			} catch(e){
				console.log(e)
			}
		}
	}
	const alertForNotValid = () => {
    Alert.alert(
      "Check your answer",
      "You have to answer all of the questions"
    ),[
      {
        text: "Cancel",
        onPress: () => console.log("Cancel Pressed"),
        style: "cancel"
      }
    ]
	}
	return (
    <View style={{backgroundColor:'#fff', flex:1}}>
			<View style={{margin: width * 0.05}}>
				<View style={{flexDirection:'row'}}>
					<Text style={{color:"#999", textDecorationLine:'underline'}}>{title}</Text>
          <Text style={{color:'#000'}}> / </Text>
					<Text style={{color: "#3E89AE", fontWeight:'bold'}}>Final Assessment</Text>
				</View>
				<View style={{marginTop: width * 0.02, borderBottomWidth: 1, borderColor: "#e9e9e9", padding: width * 0.02}}>
					<Text style={{fontSize: height * 0.021, fontWeight:'bold'}}>Final Assessment</Text>
					<Text style={{fontWeight:'bold'}}>{question.length} Questions</Text>
				</View>
			</View>
    	<FlatList
				data={question}
				ListEmptyComponent={<ActivityIndicatorModal visible={visible}/>}
				keyExtractor={item=>item._id}
				renderItem={({item, index})=>{
					return(
						<View style={{borderBottomWidth: 1, borderColor: "#e9e9e9", marginBottom:height * 0.01}}>
							<Text style={{fontSize:height * 0.021, margin: width * 0.02}}>{item.question}</Text>
							<View style={{marginLeft: width * 0.05}}>
								<Text style={{color:"#999", fontWeight:'bold', margin: width * 0.02}}>Answer:</Text>
								<OptionsCard
									question_id={item._id}
									index={index}
									onClicked={(index, value)=>{
										getAnswer(index, value)
									}}
									target={answer}
								/>
							</View>
						</View>
					)
				}}
			/>
			<View style={{margin:10}}>
				<TouchableOpacity style={{...STYLES.buttonBlue}}
					onPress={async ()=>{
						if(answer.includes("")){
							alertForNotValid()
						} else {
							await uploadAnswer()
							await navigation.goBack()
						}
					}}
				>
					<Text style={{...STYLES.textWhite, fontWeight:'bold'}}>Submit Assesstment</Text>
				</TouchableOpacity>
			</View>
    </View>
)};

export default TestPage;
