import React, {useState, useEffect} from 'react';
import { Text, View, Image, Dimensions, FlatList, TouchableOpacity} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import {useSelector, useDispatch} from 'react-redux';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import LinearGradient from 'react-native-linear-gradient'

import { detailCourse, contentCourse, enrollClass, getLessonsCourse, deleteLessonCourse} from '../../store/action'

import STYLES from '../../styles/style'
import ContentComponent from '../../components/ContentComponent'
import VirtualizedView from '../../components/VirtulalizedView'
import CoursesCategory from '../../components/CoursesCategory'
import EnrollSuccessModal from './EnrollSuccessModal'
import ActivityIndicatorModal from './ActivityIndicatorModal';

const width = Math.round(Dimensions.get("screen").width)
const height = Dimensions.get("screen").height

const CourseDetails = ({
  navigation, route
}) => {
	const {id} = route.params;
	const dispatch = useDispatch()
	const detail = useSelector(state=>state.detail)
	const listCourse = useSelector(state => state.enrolledCourse)
	const content = useSelector(state=>state.content)
	const token = useSelector(state => state.user.token)
	const [seeMore, setSeeMore]=useState(true)
	const [isTruncatedText, setIsTruncatedText] = useState(false)
	const categoryCourse = useSelector(state =>state.category)

	const enrolled = listCourse.find(course => course.course.id == id)
	
	const [visible, setVisible] = useState(true)
	let index = 0
	let status = ''
	console.log(enrolled)
	if(enrolled){
		index = listCourse.findIndex(course => course.course.id == id)
		status = enrolled.status
	}
	useEffect(() => {
		if(typeof detail.title == 'undefined'){
			setVisible(true)
		} else {
			setVisible(false)
		}
	}, [detail.title]);

	if(typeof detail.title == 'undefined'){
		return null
	}

  return (
		<SafeAreaView style={{flex: 1}}>
			<VirtualizedView>
			<LinearGradient
				colors={["#000", '#fff']}
				start={{ x: 0.5, y: 0 }}
				end={{ x: 1, y: 1 }}
				style={{...STYLES.linearGradient, position: 'absolute',}}
			>
				<Image source={{uri: detail.image}} style={{width: "100%", height:"100%", opacity:0.4}}/>
			</LinearGradient>
			<View style={{position:'absolute', padding: width * 0.02, flexDirection:'row', justifyContent:'space-between', width: width}}>
				<TouchableOpacity onPress={()=>navigation.goBack()}>
					<MaterialIcons name="arrow-back-ios" size={ height * 0.028 } color="#fff" />
				</TouchableOpacity>
				<View style={{flexDirection:'row', width: width*0.2, justifyContent:'space-between'}}>
					<TouchableOpacity>
						<MaterialIcons name="favorite-outline" size={ height * 0.028 } color="#fff" />
					</TouchableOpacity>
					<TouchableOpacity>
						<MaterialIcons name="share" size={ height * 0.028 } color="#fff" />
					</TouchableOpacity>
				</View>
			</View>
			<View style={{marginTop: height * 0.08, padding: width * 0.02}}>
				<Text style={{fontSize:height * 0.03, fontWeight:'bold', color:'#fff'}}>{detail.title}</Text>
				<Text style={STYLES.subText}>By {detail.user.name}</Text>
				<View style={{marginTop:10, backgroundColor: 'rgba(240,166,62,0.5)', width:width * 0.4, borderRadius: 5, alignItems:'center', padding:  width * 0.02}}>
					<Text style={{fontWeight:'bold', color:'#fff', letterSpacing: 2, fontSize: height*0.017}}>{detail.category}</Text>
				</View>
			</View>
			<View style={{ ...STYLES.shadow,marginHorizontal: width* 0.08, marginTop: height*0.05, backgroundColor:'#fff', borderWidth: 1, padding:  width * 0.03, borderColor: '#E5E5E5' }}>
				<View style={{flexDirection:'row', justifyContent:'space-around', marginBottom:14}}>
					<View style={{alignItems:'center'}}>
						<Text style={{fontSize:height * 0.05, color:'rgba(239,156,39,1)'}}>{detail.lesson}</Text>
						<Text>Learning Videos</Text>
					</View>
					<View style={{alignItems:'center'}}>
						<Text style={{fontSize:height * 0.05, color:'rgba(239,156,39,1)'}}>{detail.material}</Text>
						<Text>Study Material</Text>
					</View>
				</View>
				<View style={{ borderColor:'#E5E5E5', borderWidth: 1}}/>
				<View style={{marginTop:height*0.01}}>
					{isTruncatedText ?
						<>
						<Text style={{fontSize: height * 0.022}} numberOfLines={seeMore ? 5 : 0}>{detail.description}</Text>
						<Text onPress={()=>setSeeMore(!seeMore)} style={{fontWeight:'bold'}}>
						{seeMore ? 'Read More' : 'Less'}
						</Text>
						</>
						:
						<Text 
							style={{fontSize: height * 0.022}}
							numberOfLines={5}
							onTextLayout={(event)=>{
								const {lines} = event.nativeEvent
								setIsTruncatedText(lines?.length > 5)
							}}
						>
							{detail.description}
						</Text>
					}
					
				</View>
			</View>
			<View style={{padding:  width * 0.04, marginBottom:height*0.01}}>
					<Text style={{fontSize: 18, fontWeight:'bold', marginBottom:height*0.01}}>Content</Text>
					<FlatList 
						data={content}
						keyExtractor={(item) => item.id}
						renderItem={({item, index})=>{
							return (
								<ContentComponent
									title={item.title}
									description={item.description}
								/>
							)
						}}
					/>
			</View>
			<View style={{marginBottom: height*0.1, padding:  width * 0.01,}}>
				<Text style={{fontSize:height*0.025, fontWeight:'bold'}}>Related Course</Text>
				<FlatList 
					nestedScrollEnabled
					horizontal
					showsHorizontalScrollIndicator={false}
					data={categoryCourse}
					keyExtractor={(item)=>item.id}
					renderItem={({item}) => {
						if(item.id == id){
							return null
						}
						return (
							<CoursesCategory 
								title={item.title}
								imageUrl={item.image}
								teacher={item.user.name}
								totalStudent={item.enrolled}
								totalVideos={item.lesson}
								description={item.description}
								category={item.category}
								totalLesson={item.material}
								id={item.id}
								onClicked={ ()=> {
									dispatch(detailCourse(item.id))
									dispatch(contentCourse(item.id))
								}}
							/>
						)
					}}
				/>
				</View>
			</VirtualizedView>
			<View style={{flexDirection:'row', justifyContent: 'space-between',position:'absolute', backgroundColor:'#fff', bottom:0, right: 0, left:0, width: width, padding:  width * 0.02}}>
				<View>
					<Text style={{fontWeight:'bold'}}>{detail.title}</Text>
					<Text style={{color:'rgba(153,153,153,1)'}}>{detail.user.name}</Text>
				</View>
				<View>
				{enrolled && (status == 'active' || status =='completed') &&
					<TouchableOpacity 
						style={{...STYLES.buttonOrange,width: width* 0.25, height: height* 0.05, borderRadius: 10, padding: 10}}
						onPress={()=>{
							dispatch(getLessonsCourse({id: id, token: token}))
							navigation.navigate('CoursePlay', {id: id, index: 0, title:detail.title, poster:detail.image})
						}}
					>
						<Text style={{...STYLES.textWhite, fontSize: 14}}>
							Go to Course
						</Text>
					</TouchableOpacity>
				}
				{enrolled && status == 'pending' &&
					<TouchableOpacity 
						style={{...STYLES.buttonOrange,width: width* 0.25, height: height* 0.05, borderRadius: 10, backgroundColor:'grey'}}
					>
						<Text style={{...STYLES.textWhite, color:'#000', fontSize: 14}}>
							Pending
						</Text>
					</TouchableOpacity>
				}
				{!enrolled && 
				<TouchableOpacity 
					style={{...STYLES.buttonOrange,width: width* 0.25, height: height* 0.05, borderRadius: 10}}
					onPress={()=>{
						dispatch(enrollClass({id:id, teacher: detail.user.name, title: detail.title, token:token, image:detail.image}))
					}}	
				>
					<Text style={{...STYLES.textWhite, fontSize: 14}}>
						Enroll
					</Text>
				</TouchableOpacity>
				}
				</View>
				<EnrollSuccessModal/>
			</View>
			<ActivityIndicatorModal 
				visible={visible}
			/>
		</SafeAreaView>
)};

export default CourseDetails;
