import React, {useEffect, useState} from 'react';
import { Text, View, Dimensions, FlatList, TouchableOpacity, Alert } from 'react-native';
import {useSelector, useDispatch} from 'react-redux';

import authAPI from '../../api/auth'
import AnsweredCard from '../../components/AnsweredCard';
import STYLES from '../../styles/style';
import ActivityIndicatorModal from './ActivityIndicatorModal';

const width = Math.round(Dimensions.get("screen").width)
const height = Dimensions.get("screen").height

const ResultPage = ({
  route, navigation
}) => {
	const {test_id, title, student_test_id, score, totalQuestion, totalCorrect} = route.params
	const token = useSelector(state => state.user.token)
	const [question, setQuestion] = useState([])
	const [answer, setAnswer] = useState([])
	const [visible, setVisible] = useState(true)
	useEffect(() => {
		async function getQuestion(){
			try{
				const response = await authAPI.get(`/questions/byTest/${test_id}`, {'headers': { 'Authorization': `Bearer ${token}`}})
				setQuestion(response.data.data)
				const responseAnswer = await authAPI.get(`/studentTests/student/test/${test_id}`, {'headers': { 'Authorization': `Bearer ${token}`}})
				setAnswer(responseAnswer.data.data)
				setVisible(false)
			} catch(e){
				console.log(e)
			}
		}
		getQuestion()
	}, []);

	if(typeof answer[0] === 'undefined' || question == []){
		return <ActivityIndicatorModal 
		visible = {visible}
	/>
	}
	// if(question == []){
	// 	return null
	// }
	return (
    <View style={{backgroundColor:'#fff', flex:1}}>
			<View style={{margin: width * 0.05}}>
				<View style={{flexDirection:'row'}}>
					<Text style={{color:"#999", textDecorationLine:'underline'}}>{title}</Text>
          <Text style={{color:'#000'}}> / </Text>
					<Text style={{color: "#3E89AE", fontWeight:'bold'}}>Final Assessment</Text>
				</View>
				<View style={{marginTop: width * 0.02, padding: width * 0.02}}>
					<Text style={{fontSize: height * 0.021, fontWeight:'bold'}}>Final Assessment</Text>
				</View>
				<View style={{marginTop: width * 0.02, padding: width * 0.02, justifyContent:'center', alignItems:'center'}}>
					<Text style={{...STYLES.textOrange, fontSize: height*0.035}}>{score}%</Text>
					<Text style={{color:'#999'}}>{totalCorrect}/{totalQuestion} Question Correct</Text>
				</View>
				<View style={{marginTop: width * 0.02, borderBottomWidth: 1, borderColor: "#e9e9e9", padding: width * 0.02}}>
					<Text style={{fontWeight:'bold'}}>{question.length} Questions</Text>
				</View>
			</View>
    	<FlatList
				data={question}
				keyExtractor={item=>item._id}
				renderItem={({item, index})=>{
					return(
						<View style={{borderBottomWidth: 1, borderColor: "#e9e9e9", marginBottom:height * 0.01}}>
							<Text style={{fontSize:height * 0.021, margin: width * 0.02}}>{item.question}</Text>
							<View style={{marginHorizontal: width * 0.05}}>
								<View style={{flexDirection:"row", justifyContent: 'space-between', alignItems:'center'}}>
									<Text style={{color:"#999", fontWeight:'bold', margin: width * 0.02}}>Answer:</Text>
									{answer[index].answer.answer == false ? <Text style={{color:"#DD5571", fontWeight:'bold'}}>Wrong</Text> :
									<Text style={{color:"#61D3A4", fontWeight:'bold'}}>Correct</Text>
									}
								</View>
								<AnsweredCard
									question_id={item._id}
									remark={answer[index].question.remark}
									state_answer={answer[index].answer.answer}
									student_answer_id={answer[index].answer.id}
								/>
							</View>
						</View>
					)
				}}
			/>
    </View>
)};

export default ResultPage;
