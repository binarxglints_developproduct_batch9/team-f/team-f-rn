import React, {useEffect} from 'react';
import { Text, View, FlatList, Dimensions } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import {useSelector, useDispatch} from 'react-redux';

import VirtualizedView from '../../components/VirtulalizedView'
import AssignmentCard from '../../components/AssignmentCard'
import {getEnrolledCourse, getTestResult} from '../../store/action'

const width = Math.round(Dimensions.get("screen").width)
const height = Dimensions.get("screen").height

const AssignmentListPage = ({
  navigation,
}) => {
	const listCourse = useSelector(state => state.enrolledCourse)
	const token = useSelector(state => state.user.token)
	const test = useSelector(state => state.test)
	const dispatch = useDispatch()
	useEffect(() => {
    const listener = navigation.addListener('focus', () => {
			dispatch(getEnrolledCourse({token}))
			dispatch(getTestResult(token));
    });
    return listener    
	}, []);
	const activeCourse = listCourse.filter(course => {
    return course.status === 'active'
  })
	const takeQuizCourse = activeCourse.filter(course => {
    return course.totalLesson === course.totalLessonCompleted
	})
	const completeCourse = listCourse.filter(course => {
		return course.status === 'completed'
	})
	const temp = [...takeQuizCourse, ...completeCourse]
	const result = []

	for(let i = 0; i < temp.length; i++){
		for(let j = 0; j < test.length; j++){
			if(temp[i].id == test[j].enroll){
				result.push({...temp[i], ...test[j]})
				break;
			}
		}
	}
	if(listCourse.length == 0){
		return (
			<SafeAreaView style={{flex:1, justifyContent:'center', alignItems:'center'}}>
				<Text>Enroll and finish your current course and get certificate!</Text>
			</SafeAreaView>
		)
	}

  return (
		<FlatList 
			data={result}
			keyExtractor={item=>item.id}
			renderItem={({item})=> {
				return (<AssignmentCard 
					item={item}
				/>)
			}}
		/>
)};

export default AssignmentListPage;
