import React from 'react';
import { Text, View, FlatList } from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import ListMaterials from '../../components/ListMaterials';

const CourseMaterial = ({
    navigation,
}) => {
  const lessons = useSelector(state=>state.lesson)
  if(lessons.length == 0){
    return null
  }
  return (
    <View>
      <FlatList
        data={lessons}
        keyExtractor={(item)=> item._id}
        renderItem={({item})=>{
          return(
            <View>
              <Text style={{fontSize: 18, fontWeight:'bold'}}>{item.lesson.title}</Text>
              <ListMaterials 
                data={item.lesson.material}
              />
            </View>
          )
        }}
      />
    </View>
)};

export default CourseMaterial;
