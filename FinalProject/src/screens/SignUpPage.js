import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Dimensions,
  Alert,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';

import {registerQuest, clearError, clearStatus} from '../store/action'
import STYLES from '../styles/style'
import ActivityIndicatorModal from './Students/ActivityIndicatorModal';

const height = Dimensions.get("screen").height


const SignUpPage = ({navigation, route}) => {
  const [Name, setName] = useState('')
  const [Email, setEmail] = useState('');
  const [Password, setPassword] = useState('')
  const [ConfirmPassword, setConfirmPassword] = useState('')
  const [validation, setValidation] = useState({
    validName: true,
    validPassword: true,
    validEmail:true
  })
  const [validConfirm, setValidConfirm] = useState(true)
  const emailExisted = useSelector(state=>state.user.errorMsg)
  const user = useSelector(state=>state.user)
  const {role} = route.params
  const [show, setShow] = useState(false)
  const dispatch = useDispatch();
  const handleValidPassword = (value) => {
    if(value.length >= 8 && value.length <= 32){
      setValidation({
        ...validation,
        validPassword:true
      })
    } else {
      setValidation({
        ...validation,
        validPassword:false
      })
    }
  }
  const handleValidConfirmPassword = (value) => {
    if(value!= Password){
      setValidConfirm(false)
    } else {
      setValidConfirm(true)
    }
  }
  const handleValidName = (value) => {
    if(value.trim().length >= 4){
      setValidation({
        ...validation,
        validName:true
      })
    } else {
      setValidation({
        ...validation,
        validName:false
      })
    }
  }
  const handleValidEmail = (value) => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if(reg.test(value) == false){
      setValidation({
        ...validation,
        validEmail:false
      })
    } else {
      setValidation({
        ...validation,
        validEmail:true
      })
    }
  }
  const alertForNotValid = () => {
    Alert.alert(
      "Check your input",
      "There are something wrong in name, email, password or, confirm password"
    ),[
      {
        text: "Cancel",
        onPress: () => console.log("Cancel Pressed"),
        style: "cancel"
      }
    ]
  }
  const signUpSuccess = () => 
    Alert.alert(
      "Sign Up Success",
      "Congratulations, your account has been successfully created"
    ,[
      { text: "OK", onPress: () => navigation.navigate('LogIn', {role: role}) }
    ],
    { cancelable: false }
  )

  useEffect(() => {
    const listener = navigation.addListener('blur', () => {
      dispatch(clearError())
      dispatch(clearStatus())
    });
    return listener;
  }, [navigation]);
  
  useEffect(() => {
    if(user.status == 'Signup success!'){
      signUpSuccess()
    }
  }, [user]);
  console.log(user)
  return(
    <View style={{
      flex:1,
      marginVertical:height * 0.05,
      backgroundColor:'white',
      alignItems:'center',
      justifyContent:'center',
      }}
    >
      {role === 'student' ? <Text
        style={STYLES.headerForm}>
          Start Learning!
      </Text> :<Text
        style={STYLES.headerForm}>
          Get Involved Now!
      </Text>
      }
      <Text
        style={{
          fontSize: height*0.015,
          letterSpacing: 2,
          marginBottom: 25,
          color: '#16213B'
        }}>
        Create your account
      </Text>
      <Text
        style={STYLES.label}>
        Name*
      </Text>
      <TextInput style={STYLES.inputForm}
        autoCorrect={false}
        autoCapitalize="none"
        clearButtonMode='while-editing'
        placeholder="John Doe"
        placeholderTextColor='#999'
        value={Name}
        onChangeText={(name) => {
          setName(name);
          handleValidName(name)
        }}
      />
      <Text
        style={STYLES.label}>
        Email*
      </Text>
      <TextInput style={STYLES.inputForm} 
        placeholder="john@doe.com"
        placeholderTextColor='#999'
        value={Email}
        onChangeText={(email) => {
          setEmail(email);
          handleValidEmail(email)
        }}
        autoCapitalize="none"
        autoCorrect={false}
      />
      {validation.validEmail ? null : <Text style={STYLES.errorStyle}>Email is not valid</Text>}
      {emailExisted ? <Text style={STYLES.errorStyle}>{emailExisted}</Text> : null}
      <Text
        style={STYLES.label}>
        Password*
      </Text>
      <TextInput style={STYLES.inputForm}
        placeholder="Password Here"
        secureTextEntry
        value={Password} 
        placeholderTextColor='#999'
        onChangeText={(password) => {
          setPassword (password);
          handleValidPassword(password)
        }} 
        autoCapitalize="none"
        autoCorrect={false}
        />
        <Text
        style={STYLES.label}>
        Confirm Password*
      </Text>
        <TextInput style={STYLES.inputForm}
        placeholder="Confirm Password Here"
        secureTextEntry
        value={ConfirmPassword} 
        placeholderTextColor='#999'
        onChangeText={(password) => {
          setConfirmPassword (password);
          handleValidConfirmPassword(password)
        }} 
        autoCapitalize="none"
        autoCorrect={false}
        />
        {validation.validPassword == true ? null : <Text style={STYLES.errorStyle}>Password have to be more than 8 and less than 32 character</Text>}
       <TouchableOpacity style={STYLES.buttonForm}
        onPress={async () => {
          if(validation.validEmail && validation.validName && validation.validPassword&& validConfirm && Email.length !=0 && Name.length != 0 && Password.length !=0 && ConfirmPassword.length!=0){
            dispatch(registerQuest({role: role, name: Name, email:Email, password: Password}))
          }else{
            alertForNotValid()
          }
         }}
        >
        <Text style={STYLES.textForm}>
          Sign Up
        </Text>
      </TouchableOpacity>
      <TouchableOpacity>
        <Text style={STYLES.authFont}
          onPress={() => navigation.navigate('LogIn', {role: role})}>
          Already have account? Login
        </Text>
      </TouchableOpacity>
      <ActivityIndicatorModal 
        visible={user.loading}
      />
    </View>
	)
}

export default SignUpPage;