import React, {useEffect} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import STYLES from '../styles/style';
const width = Math.round(Dimensions.get("screen").width)
const height = Dimensions.get("screen").height

const RolePage = ({navigation}) => {
  return(
    <View style={{
      flex:1,
      marginVertical:height * 0.1,
      backgroundColor:'white',
      alignItems:'center',
      justifyContent:'center',
      flexDirection:"row",
      justifyContent:"space-around"
      }}> 
      <TouchableOpacity style={{
        ...STYLES.buttonForm,
        height:height*0.06,
        alignItems:'center',
        justifyContent:'center',
        borderRadius:25,
      }}
        onPress={() => navigation.navigate('SignUp', {role:'teacher'})}>
        <Text style={STYLES.textForm}>
          I am Teacher
        </Text>
      </TouchableOpacity>  
      <TouchableOpacity style={{
        ...STYLES.buttonForm,
        height:height*0.06,
        alignItems:'center',
        justifyContent:'center',
        borderRadius:25,
      }}
        onPress={() => navigation.navigate('SignUp', {role: 'student'})}>
        <Text style={STYLES.textForm}>
          I am Student
        </Text>
      </TouchableOpacity>  
    </View>
  )
}

export default RolePage;