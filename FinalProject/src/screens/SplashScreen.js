import React from 'react';
import { Text, View, Image} from 'react-native';

const SplashScreen = ({
    params,
}) => {

  return(
    <View style={{flex:1, 
      alignItems:'center',
      justifyContent:'center',}}>
      <Image source={require('../assets/background.jpg')} style={{position: "absolute", width: "100%", height:"100%"}}/>
      <View style={{
        backgroundColor:'#ffffff',
        shadowOpacity: 0.26,
        shadowOffset: {width: 0, height: 2},
        shadowRadius: 8,
        elevation: 5,
        width: "80%",
        height: "80%",
        alignItems:'center',
        justifyContent:'center',
      }}
      >
        <View style={{borderBottomWidth: 4, borderColor: "#EF9C27"}}>
          <Text style={{fontSize: 50}}>LEKTUR</Text>
        </View>     
      </View>
    </View>
)};

export default SplashScreen;
