import ACTION from '../types'

const initialState = []

export default function testResultReducer (state=initialState, action){
  const {type, payload} = action
  switch(type){
    case ACTION.PUT_TEST_RESULT:
      return payload
    default:
      return state
  }
}