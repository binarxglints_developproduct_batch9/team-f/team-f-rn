import ACTION from '../types'

const initialState = {}

export default function detailReducer (state = initialState, action){
  const {type, payload} = action
  switch(type) {
    case ACTION.DELETE_DETAIL_COURSE:
      return {}
    case ACTION.PUT_DETAIL_COURSE:
      return payload
    default:
      return state
  }
}

