import ACTION from '../types'

const initialState = []

export default function lessonReducer(state = initialState, action){
  const {type, payload} = action
  switch(type){
    case ACTION.PUT_LESSONS_COURSE:
      return payload
    case ACTION.DELETE_LESSONS_COURSE:
      return []
    default:
      return state
  }
}