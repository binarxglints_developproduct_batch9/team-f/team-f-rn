import ACTION from '../types'
const initialState = {
  title:'',
  teacher:'',
  status:'',
  image:'',
  visibile:false
}

export default function modalEnrollReducer (state = initialState, action){
  const {type, payload} = action
  switch(type){
    case ACTION.CLEAR_MODAL:
      return {title:'', teacher:'', status:'', image:'', visible:false}
    case ACTION.ENROLL_SUCCESS:
      return {title:payload.title, teacher:payload.teacher, status: payload.status, image:payload.image, visible: true}
    case ACTION.ENROLL_ALREADY:
      return {title:payload.title, teacher:payload.teacher, status: payload.status, image:payload.image, visible: true}
    default:
      return state
  }
}