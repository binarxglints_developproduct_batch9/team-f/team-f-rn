import ACTION from '../types'
const initialState = []

export default function searchCourseReducer (state = initialState, action){
  const {type, payload} = action
  switch(type){
    case ACTION.DELETE_SEARCH_COURSE:
      return []
    case ACTION.PUT_SEARCH_COURSE:
      return payload
    default:
      return state
  }
}