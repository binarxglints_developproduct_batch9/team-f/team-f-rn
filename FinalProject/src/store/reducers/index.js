import {combineReducers} from 'redux'
import courseTeacherReducer from './courseTeacherReducer'
import userReducer from './userReducer'
import allCourseReducer from './allCourseReducer'
import courseByCategoryReducer from './courseByCategoryReducer'
import contentReducer from './contentReducer'
import detailReducer from './detailReducer'
import modalEnrollReducer from './modalEnrollReducer'
import listStudentReducer from './listStudentReducer'
import enrolledCourse from './enrolledCourse'
import lessonReducer from './lessonReducer'
import testResultReducer from './testResultReducer'
import searchCourseReducer from './searchCourseReducer'

export default combineReducers({
  user: userReducer,
  courseTeacher: courseTeacherReducer,
  all: allCourseReducer,
  category:courseByCategoryReducer,
  content: contentReducer,
  detail: detailReducer,
  modal : modalEnrollReducer,
  listStudent : listStudentReducer,
  enrolledCourse : enrolledCourse,
  lesson: lessonReducer,
  test: testResultReducer,
  search: searchCourseReducer,
})