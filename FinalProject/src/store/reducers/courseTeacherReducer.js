import ACTION from '../types'

const initialState = []

export default function courseTeacherReducer(state = initialState, action){
  const {type, payload} = action
  switch(type){
    case ACTION.PUT_COURSE_TEACHER:
      return payload
    default:
      return state
  }
}