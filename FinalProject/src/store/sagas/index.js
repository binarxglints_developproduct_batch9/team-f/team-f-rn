import ACTION from '../types'
import {takeLatest, call, put} from 'redux-saga/effects'
import authAPI from '../../api/auth'
import AsyncStorage from '@react-native-async-storage/async-storage';
import FormData from 'form-data';
import axios from 'axios'

/**SignIn */
async function trySignIn (payload){
  const {role, email, password} = payload
  let url = '/login'
  const response = await authAPI.post(url, {email,password})
  console.log(response)
  return response.data.token
}
function* signIn({payload}){
  try{
    const response = yield call(trySignIn, payload)
    yield put ({type:ACTION.SIGNIN_SUCCESS,payload: {token:response, role:payload.role}})
  } catch (err) {
    yield put({type: ACTION.ERROR, payload: err.response.data})
  }
}
/**SignIn */

/**Sign Up */
async function trySignUp (payload){
  const {role, email, password, name} = payload
  let url = role == 'teacher' ? '/signupTeacher' : '/signup'
  const response = await authAPI.post(url, {name, email, password})
  return response.data.status
}
function* signUp({payload}){
  try{
    yield put({type:ACTION.LOADING})
    const response = yield call(trySignUp, payload)
    console.log(response)
    yield put ({type:ACTION.SIGNUP_SUCCESS,payload: response})
  } catch (err) {
    yield put({type: ACTION.ERROR, payload: 'Email have already existed'})
  }
}
/**Sign  Up*/

/**GET USER */
async function tryGetUser (payload) {
  const response = await authAPI.get('/userProfile', { 'headers': { 'Authorization': `Bearer ${payload}` }})
  await AsyncStorage.setItem('token', payload)
  await AsyncStorage.setItem('role', response.data.data.role)
  return response.data.data
}
function* getUser({payload}){
  try{
    const response = yield call(tryGetUser, payload)
    yield put ({type: ACTION.GET_USER_SUCCESS, payload: response})
  } catch(err) {
    console.log(err.message)
  }
}
/**GET USER */

/**CHECK THE ASYNCSTORAGE */
async function checking(){
  const token = await AsyncStorage.getItem('token')
  const role = await AsyncStorage.getItem('role')
  return {role, token}
}
function* checkStorage(){
  try{
    const response = yield call(checking)
    if(response.token && response.role){
      yield put({type: ACTION.SIGNIN_SUCCESS,payload: {token:response.token, role: response.role}})
    } else {
      yield put({type: ACTION.SIGNIN_SUCCESS,payload: {token:'', role: ''}})
    }
  }catch(err){
    console.log(err.message)
  }
}
/**CHECK THE ASYNCSTORAGE */

/**GET ALL COURSE */
async function gettingAllCourse(){
  const response = await authAPI.get('/courses/all')
  return response.data.data
}
function* getAllCourse () {
  try{
    const response = yield call(gettingAllCourse)
    yield put ({type:ACTION.PUT_ALL_COURSE, payload: response})
  } catch(e){
    console.log(e.message)
  }
}
/**GET ALL COURSE */

/**GET CATEGORY COURSE */
async function gettingCategoryCourse(payload){
  const response = await authAPI.get(`/courses/find/byCategory?key=${payload}`)
  return response.data.data
}
function* getCategoryCourse ({payload}) {
  try{
    const response = yield call(gettingCategoryCourse, payload)
    yield put ({type:ACTION.PUT_CATEGORY_COURSE, payload: response})
  } catch(e){
    console.log(e.message)
  }
}
/**GET CATEGORY COURSE */

/**GET DETAIL COURSE */
async function gettingDetailCourse (payload) {
  const response = await authAPI.get(`/courses/${payload}`)
  return response.data.data
}
function* getDetailCourse ({payload}){
  try{
    const response = yield call(gettingDetailCourse, payload)
    yield put({type:ACTION.PUT_DETAIL_COURSE, payload : response})
  } catch(e){
    console.log(e.message)
  }
}
/**GET DETAIL COURSE */

/**GET CONTENT COURSE */
async function gettingContentCourse(payload){
  const response = await authAPI.get(`/lessons/byCourse/${payload}`)
  return response.data.data
} 
function* getContentCourse({payload}){
  try{
    const response = yield call(gettingContentCourse, payload)
    yield put({type:ACTION.PUT_CONTENT_COURSE, payload: response})
  } catch(e){
    console.log(e.message)
  }
}
/**GET CONTENT COURSE */

/**GET COURSE BY TEACHER */
async function gettingCourseTeacher (payload){
  const response = await authAPI.get(`/courses/find/byTeacher`, { 'headers': { 'Authorization': `Bearer ${payload}`}})
  return response.data.data
}
function* getCourseTeacher({payload}){
  try{
    const response = yield call(gettingCourseTeacher, payload)
    yield put ({type:ACTION.PUT_COURSE_TEACHER, payload : response})
  } catch(e){
    console.log(e.message)
  }
}
/**GET COURSE BY TEACHER */

/**GET LIST STUDENT */
async function gettingListStudent (payload){
  const response = await authAPI.get(`/enrolls/get/byTeacherCourse/${payload.id}`, {'headers': { 'Authorization': `Bearer ${payload.token}`}})
  return response.data.data
}
function* getListedStudent({payload}){
  try{
    const response = yield call(gettingListStudent, payload)
    yield put({type: ACTION.PUT_LIST_STUDENT, payload: response})
  } catch(e){
    console.log(e.message)
  }
} 
/**GET LIST STUDENT */

/**ENROLL CLASS */
async function enrollingClass (payload){
  const {token, id} = payload
  console.log(payload)
  const response = await authAPI.post(`/enrolls/create/byStudent`,{'course_id': id}, {'headers': { 'Authorization': `Bearer ${token}`}})
  console.log(response.data.data)
  return response.data.data
}
function* enrollClass ({payload}){
  try{
    const response = yield call(enrollingClass, payload)
    yield put({type: ACTION.ENROLL_SUCCESS, payload:{title: payload.title, teacher: payload.teacher, image:payload.image, status: "Successfully enrolled"}})
  } catch(e){
    yield put({type: ACTION.ENROLL_ALREADY, payload:{title: payload.title, teacher: payload.teacher, image:payload.image, status: "You are already enrolled/finished this course"}})
  }
}
/**ENROLL CLASS */

/**Enrolled Course */
async function gettingEnrolledClass (payload){
  const {token} = payload
  console.log(payload)
  const response = await authAPI.get('/enrolls/get/byStudent', {'headers': { 'Authorization': `Bearer ${token}`}})
  return response.data.data
}
function* enrolledCourse({payload}){
  try{
    const response = yield call(gettingEnrolledClass, payload)
    yield put({type: ACTION.PUT_ENROLLED_COURSE, payload: response})
  } catch(e){
    console.log(e)
  }
}
/**Enrolled Course */

/**Lessons Course */
async function gettingLessonsCourse(payload){
  const {token, id} = payload
  const response = await authAPI.get(`/studentLessons/get/byEnrollment/${id}`, {'headers': { 'Authorization': `Bearer ${token}`}})
  return response.data.data
}
function* lessonsCourse({payload}){
  try{
    const response = yield call(gettingLessonsCourse, payload)
    yield put({type:ACTION.PUT_LESSONS_COURSE, payload: response})
  } catch(e){
    console.log(e)
  }
}
/**Lessons Course */

/**Test Result */
async function gettingTestResult(payload){
  const response = await authAPI.get(`/studentTests/`, {'headers': { 'Authorization': `Bearer ${payload}`}})
  return response.data.data
}
function* testResult({payload}){
  try{
    const response = yield call(gettingTestResult, payload)
    yield put({type:ACTION.PUT_TEST_RESULT, payload:response})
  } catch(e){
    console.log(e)
  }
}
/**Test Result */

/**Change Profile */
async function changingProfile(payload){
  let data = new FormData();
  data.append('name', payload.name)
  data.append('email', payload.email)
  data.append('image', payload.image)
  const response = await authAPI.patch(`/userProfile/update`, data, {'headers': { 'Authorization': `Bearer ${payload.token}`}})
  return response.data.data
}
function* changeProfile({payload}){
  try{
    const response = yield call (changingProfile, payload)
    yield put({type:ACTION.PUT_CHANGE_PROFILE, payload:response}) 
  } catch(e){
    console.log({...e})
  }
}
/**Change Profile */

/**Search Course */
async function searchingCourse(payload){
  const response = await authAPI.get(`/courses/find/byTitle?key=${payload}`)
  return response.data.data
}
function* searchCourse({payload}){
  try{
    const response = yield call(searchingCourse, payload)
    yield put({type:ACTION.PUT_SEARCH_COURSE, payload: response})
  }catch(e){
    console.log(e)
  }
}
/**Search Course */

export default function* rootSaga() {
  yield takeLatest(ACTION.SIGNIN_REQUESTED, signIn);
  yield takeLatest(ACTION.SIGNUP_REQUESTED, signUp);
  yield takeLatest(ACTION.GET_USER_REQUEST, getUser);
  yield takeLatest(ACTION.CHECK_STORAGE, checkStorage);
  yield takeLatest(ACTION.GET_ALL_COURSE, getAllCourse);
  yield takeLatest(ACTION.GET_CATEGORY_COURSE, getCategoryCourse);
  yield takeLatest(ACTION.GET_DETAIL_COURSE, getDetailCourse);
  yield takeLatest(ACTION.GET_CONTENT_COURSE, getContentCourse);
  yield takeLatest(ACTION.GET_COURSE_TEACHER, getCourseTeacher);
  yield takeLatest(ACTION.GET_LIST_STUDENT, getListedStudent);
  yield takeLatest(ACTION.ENROLL_CLASS, enrollClass)
  yield takeLatest(ACTION.GET_ENROLLED_COURSE, enrolledCourse)
  yield takeLatest(ACTION.GET_LESSONS_COURSE, lessonsCourse);
  yield takeLatest(ACTION.GET_TEST_RESULT, testResult);
  yield takeLatest(ACTION.REQUEST_CHANGE_PROFILE, changeProfile);
  yield takeLatest(ACTION.SEARCH_COURSE, searchCourse);
}