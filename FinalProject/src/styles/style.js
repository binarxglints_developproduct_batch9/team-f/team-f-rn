import { StyleSheet, Dimensions } from 'react-native';

const width = Math.round(Dimensions.get("screen").width)
const height = Dimensions.get("screen").height

const STYLES = StyleSheet.create({
  header:{
    flex:1,
    flexDirection:'row', 
    justifyContent:'space-between',
    paddingHorizontal:10,
    alignItems:"center",
    width: width
  },
  footer:{
    flex: 12,
  },
  errorStyle:{
    color:'red'
  },
  headerForm:{
    fontSize: height*0.03,
    letterSpacing: 3,
    fontWeight: 'bold',
    color: '#16213B'
  },
  label: {
    fontSize: height*0.015,
    letterSpacing: 2,
    fontWeight: 'bold',
    marginBottom: 5,
    color: '#16213B'
  },
  inputForm:{
    borderBottomWidth: 1,
    fontSize:height*0.015,
    color:'#000',
    padding:15,
    width:width*0.85,
    height:height*0.06,
    backgroundColor:'#FFFFFF',
    marginBottom:25
  },
  buttonForm:{
    width:width*0.4, 
    height:height*0.05, 
    backgroundColor:'#16213B',
    alignItems:'center',
    justifyContent:'center',
    marginVertical:height*0.035
  },
  textForm:{
    color:'#ffffff',
    fontSize:height*0.02
  },
  authFont:{
    color:'#3E89AE', 
    fontSize:height*0.017
  },
  headerCourseForm: {
    padding: 20
  },
  bodyCourseForm:{
    padding: 20
  },
  buttonWhite:{
    borderColor:"#EF9C27", 
    borderWidth: 1, 
    width: "30%", 
    height: height*0.047, 
    justifyContent:"center"
  },
  buttonOrange:{
    backgroundColor: "#EF9C27", 
    width: "30%", 
    height: height*0.047, 
    justifyContent:"center"
  },
  textWhite:{
    color:"#fff", 
    textAlign: "center", 
    fontSize: height*0.02
  },
  textOrange:{
    color: "#EF9C27", 
    textAlign: "center", 
    fontSize: height*0.02
  },
  titleForm: {
    borderBottomWidth: 1,
    fontSize: height*0.02,
    letterSpacing: 3,
    color: '#16213B',
    marginBottom: 10,
    paddingLeft: 10
  },
  overviewForm: {
    borderBottomWidth: 1,
    fontSize: height*0.02,
    height:height*0.25,
    letterSpacing: 3,
    color: '#16213B',
    paddingLeft: 10
  },
  subText:{
    fontStyle:"italic",
    color:'#a9a9a9'
  },
  linearGradient:{
    alignItems: 'center',
    justifyContent: 'center',
    height: height*0.4,
    width: width,
  },
  shadow: {
    shadowColor: 'black',
    shadowOpacity: 0.26,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 8,
    elevation: 5,
  },
  nameList:{
    color:'#000',
    fontSize: 17,
    fontWeight: 'bold',
    fontSize:17
  },
  active: {
    fontSize: 15,
    fontWeight: 'bold',
    color: '#61D3A4',
  },
  status:{
    fontSize: 11,
    color: '#a0a0a0'
  },
  score:{
    fontSize: 24,
    color: '#EF9C27'
  },
  completed:{
    fontSize: 15,
    fontWeight: 'bold',
    color: '#3E89AE',
  },
  pending:{
    marginBottom: 15,
    fontSize: 15,
    fontWeight: 'bold',
    color: '#a0a0a0',
  },
  listStudent:{
    flexDirection: 'row' ,
    justifyContent:'space-between', 
    padding: width * 0.02, 
    borderBottomWidth: 1,
    borderColor: "#E5E5E5",
  },
  buttonBlue:{
    width: width * 0.8,
    backgroundColor: '#3E89AE',
    alignSelf: 'center',
    alignItems: 'center',
    height: height * 0.056,
    justifyContent:'center',
    flexDirection:'row'
  },
  emailList:{
    color:'#3E89AE',
    fontSize:height*0.02
  }

})
export default STYLES