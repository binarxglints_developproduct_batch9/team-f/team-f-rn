class CourseList {
  constructor(id, imageUrl, totalStudent, title, teacher, totalVideos,totalLesson, description, category){
    this.id = id;
    this.imageUrl = imageUrl;
    this.totalStudent = totalStudent;
    this.title = title;
    this.teacher = teacher;
    this.totalVideos = totalVideos;
    this.totalLesson = totalLesson;
    this.description = description;
    this.category = category;
  }
}
export default CourseList