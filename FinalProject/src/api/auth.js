import axios from 'axios';

export default axios.create({
  baseURL: 'https://lektur.kuyrek.com'
})