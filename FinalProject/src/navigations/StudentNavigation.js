import 'react-native-gesture-handler';
import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import AssignmentListPage from '../screens/Students/AssignmentListPage'
import CourseDetails from '../screens/Students/CourseDetails'
import CourseListPage from '../screens/Students/CourseListPage'
import CoursePlayPage from '../screens/Students/CoursePlayPage'
import HomePage from '../screens/Students/HomePage'
import ResultPage from '../screens/Students/ResultPage'
import TestPage from '../screens/Students/TestPage'
import StudentProfilePage from '../screens/Students/StudentProfilePage';
import EditProfilePage from '../screens/Students/EditProfilePage';
import CourseMaterial from '../screens/Students/CourseMaterial'
import CourseContent from '../screens/Students/CourseContent'
import PDFPage from '../screens/Students/PDFPage'
import SearchPage from '../screens/Students/SearchPage'

const CourseStack = () => {
  const Stack = createStackNavigator();
  return (
    <Stack.Navigator>
      <Stack.Screen name="CourseList" component={CourseListPage}/>
    </Stack.Navigator>
  )
}

const AssignmentStack = () => {
  const Stack = createStackNavigator();
  return (
    <Stack.Navigator>
      <Stack.Screen name="Assignment" component={AssignmentListPage}
        options={{title: "Assignment", headerTitleAlign:"center"}}
      />
    </Stack.Navigator>
  )
}

const HomeTabNavigation = () => {
  const Tab = createBottomTabNavigator();
  return (
    <Tab.Navigator
    tabBarOptions={{
      activeTintColor: "#000",
      labelStyle:{
        fontSize: 12,
        fontWeight:"bold"
      }
    }}
    >
      <Tab.Screen 
      name="HomePage" 
      component={HomePage}
      options = {{
        tabBarIcon: ({ focused}) => (
          <MaterialIcons name="home" color={focused? "#EF9C27" : "#999999"} size={focused? 35: 25} />
        ),
        tabBarLabel: "Home",
        headerShown: false
      }}
    />
      <Tab.Screen 
        name="CourseStack" 
        component={CourseStack}
        options = {{
          tabBarIcon: ({ focused}) => (
            <MaterialIcons name="library-books" color={focused? "#EF9C27" : "#999999"} size={focused? 35: 25} />
          ),
          tabBarLabel: "Course"
        }}  
      />
      <Tab.Screen 
        name="AssignmentStack"
        component={AssignmentStack}
        options = {{
          tabBarIcon: ({ focused}) => (
            <MaterialIcons name="edit" color={focused? "#EF9C27" : "#999999"} size={focused? 35: 25} />
          ),
          tabBarLabel: "Assignment"
        }}   
      />
      <Tab.Screen 
        name="StudentProfile" 
        component={StudentProfilePage}
        options = {{
          tabBarIcon: ({ focused}) => (
            <MaterialIcons name="person" color={focused? "#EF9C27" : "#999999"} size={focused? 35: 25} />
          ),
          tabBarLabel: "Profile"
        }}   
      />
    </Tab.Navigator>
  )
}

const StudentNavigation = () => {
  const Stack = createStackNavigator()
  return (
    <Stack.Navigator>
      <Stack.Screen 
        name="HomeTab" 
        component={HomeTabNavigation}
        options={{headerShown: false}}
      />
      <Stack.Screen 
        name="EditProfile" 
        component={EditProfilePage}
        options={{headerTitleAlign:"center", title: "Edit Profile"}}
      />
      <Stack.Screen 
        name="CourseDetails" 
        component={CourseDetails}
        options={{headerShown: false}}  
      />
      <Stack.Screen 
        name="CoursePlay" 
        component={CoursePlayPage}
        options={{headerShown: false}}
      />
      <Stack.Screen name="PDF" component={PDFPage}/>
      <Stack.Screen name="CourseMaterial" component={CourseMaterial}/>
      <Stack.Screen name="CourseContent" component={CourseContent}/>
      <Stack.Screen name="TestPage" component={TestPage}
        options={{title: "Assessment", headerTitleAlign:"center"}}
      />
      <Stack.Screen name="Result" component={ResultPage}
        options={{title: "Assessment", headerTitleAlign:"center"}}
      />
      <Stack.Screen name="SearchPage" component={SearchPage}/>
    </Stack.Navigator>
)};

export default StudentNavigation;
