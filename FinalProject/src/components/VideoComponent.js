import React from 'react';
import { Text, View, Pressable, Dimensions } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { FlatList } from 'react-native-gesture-handler';

const width = Math.round(Dimensions.get("screen").width)
const height = Dimensions.get("screen").height

const VideoComponent = ({
  data, onClicked, target
}) => {
  return(
    <FlatList 
      data={data}
      keyExtractor={(item, index) => `${index} ${item.lesson._id}`}
      listKey={(item, index) => `${index} ${item.lesson._id}`}
      renderItem={({item, index})=>{
        return(
          <Pressable
            style={({ pressed }) => [
              {
                backgroundColor: pressed
                  ? 'rgb(210, 230, 255)'
                  : !item.isPermission ? '#F5F5F5' : target == index ? "rgb(210, 230, 255)" : '#fff'
              }, {flexDirection:'row', borderColor:'rgba(229,229,229,1)', marginBottom: 1, borderBottomWidth: 1, padding: width * 0.05, alignItems:'center',}
            ]}
            onPressOut={()=>onClicked(index)}
            disabled={item.isPermission ? false : true}
          >
            <MaterialIcons name={item.isPermission ? "play-arrow" : 'lock'} size={24}/>
            <Text>{item.lesson.title}</Text>
          </Pressable>
        )
      }}
    />
)};

export default VideoComponent;
