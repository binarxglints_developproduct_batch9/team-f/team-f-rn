import React, {useState, useEffect} from 'react';
import { Text, View, FlatList, Dimensions } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import {useSelector, useDispatch} from 'react-redux';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import authAPI from '../api/auth';

const width = Math.round(Dimensions.get("screen").width)
const height = Dimensions.get("screen").height

const OptionsCard = ({
  index, question_id, onClicked, target
}) => {
  const [options, setOptions] = useState([])
  const token = useSelector(state => state.user.token)
  const [selected, setSelected] = useState(true)
  useEffect(()=>{
    async function getOptions(){
			try{
        const response = await authAPI.get(`/questions/one/${question_id}`, {'headers': { 'Authorization': `Bearer ${token}`}})
        console.log(response)
				setOptions(response.data.option)
			} catch(e){
				console.log(e)
			}
		}
		getOptions()
  },[])

  return (
    <View>
      <FlatList
        data={options}
        extraData={selected}
        renderItem={({item})=>{
          return(
            <TouchableOpacity
              onPress={()=>{
                onClicked(index, item.id)
                setSelected(!selected)
              }}
              style={{flexDirection:'row', margin: width * 0.01, alignItems:'center',}}
            >
              {target[index] == item.id ? <MaterialIcons name="radio-button-checked" size={15} style={{color:"#3E89AE"}}/>:
              <MaterialIcons name="radio-button-unchecked" size={15} style={{color:"#000"}}/>
              }
              <Text style={{fontWeight: target[index] == item.id ? 'bold': null, marginLeft: width * 0.02, fontSize: height * 0.017}}>{item.option}</Text>
            </TouchableOpacity>
          )
        }}
      />
    </View>
)};

export default OptionsCard;
