import React from 'react';
import { Text, View, Dimensions, Image } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
import STYLES from '../styles/style';

const width = Math.round(Dimensions.get("screen").width)
const height = Dimensions.get("screen").height

const SearchResult = ({
  id, title, teacher,description, image, onClicked
}) => {
  const navigation = useNavigation()
  return (
    <TouchableOpacity style={{padding: width * 0.02, margin: width * 0.02, backgroundColor:'white',...STYLES.shadow}}
      onPress={()=>{
        onClicked()
        navigation.navigate('CourseDetails', {id: id})}
      }
    >
      <View>
        <Image 
          source={{uri:image}}
          style={{
            width:"100%",
            height:200,
          }}
        />
        <View style={{flex:1}}>
          <Text style={{fontSize:height * 0.02, fontWeight:'bold'}}>{title}</Text>
          <Text style={{borderBottomWidth:1, borderColor:"#e9e9e9"}}>Instructor: {teacher}</Text>
          <Text style={{marginTop: 10}}>Description:</Text>
          <Text style={{flexShrink:1, fontWeight: 'bold'}}>{description}</Text>
        </View>
      </View>
    </TouchableOpacity>
)};

export default SearchResult;
