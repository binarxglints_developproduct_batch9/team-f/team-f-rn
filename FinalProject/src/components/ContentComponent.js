import React, {useState} from 'react';
import { Text, View, Dimensions, Pressable } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { useNavigation } from '@react-navigation/native';

const width = Math.round(Dimensions.get("screen").width)
const height = Dimensions.get("screen").height

const ContentComponent = ({
  title, description
}) => {
  const navigation = useNavigation()
  const [open, setOpen] = useState(false)
  return (
    <Pressable
      onPress={()=>{
        setOpen(!open)
      } }
      style={({ pressed }) => [
        {
          backgroundColor: pressed
            ? 'rgb(210, 230, 255)'
            : 'white'
        }, {flexDirection:'row', borderColor:'rgba(229,229,229,1)', marginBottom: 1, borderBottomWidth: 1, padding: width * 0.05,}
      ]}>
      <MaterialIcons name= {!open ? "keyboard-arrow-down" : "keyboard-arrow-up"} size={20} />
      <View style={{paddingHorizontal: 10}}>
        <Text style={{ fontWeight:'bold', fontSize: height * 0.022}}>{title}</Text>
        {open&&<Text>{description}</Text>}
      </View>
    </Pressable>
)};

export default ContentComponent;
