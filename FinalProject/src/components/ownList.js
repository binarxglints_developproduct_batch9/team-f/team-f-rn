import React from 'react';
import { Text, View, Image, TouchableOpacity, Dimensions} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import {useSelector, useDispatch} from 'react-redux'

import {getListStudent} from '../store/action'

import STYLES from '../styles/style'

const width = Math.round(Dimensions.get("screen").width)
const height = Dimensions.get("screen").height

const OwnListComponent = ({
  id, imageUrl, titleCourse, totalVideos, totalLesson, totalStudent
}) => {
  const navigation = useNavigation()
  const dispatch = useDispatch()
  const token = useSelector(state=>state.user.token)
  return(
    <View style={{borderBottomWidth: 1, marginBottom: height * 0.03, borderColor:'#E5E5E5'}}>
      <TouchableOpacity style={{height: height*0.35, width: "100%", marginBottom: height*0.02}}>
        <Image source={{uri:imageUrl}} style={{height: "80%", width: "100%"}}/>
        <View style={{marginLeft: width * 0.05}}>
          <Text style={{fontSize: height*0.02, fontWeight:'bold'}}>{titleCourse}</Text>
          <Text>{totalVideos} Videos | {totalLesson} Lessons Material</Text>
          <Text>{totalStudent} Students enrolled</Text>
        </View>
      </TouchableOpacity>
      <View style={{flexDirection: 'row', justifyContent:"space-around", marginBottom: height * 0.02}}>
        <TouchableOpacity 
          style={STYLES.buttonWhite}
          onPress={()=>{
            dispatch(getListStudent({id: id, token: token}))
            navigation.navigate('InviteStudent', {"course_id": id})
          }}
        >
          <Text style={{...STYLES.textOrange, fontWeight:'bold', fontSize: 14}}>Invite</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={STYLES.buttonOrange}
          onPress={()=>{
            dispatch(getListStudent({id: id, token: token}))
            navigation.navigate('ListStudent')
          }}
        >
          <Text style={{...STYLES.textWhite, fontWeight:'bold', fontSize: 14}}>Your Student</Text>
        </TouchableOpacity>
      </View>
    </View>
)};

export default OwnListComponent;
