import React from 'react';
import { Text, Dimensions, FlatList, TouchableOpacity } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { useNavigation } from '@react-navigation/native';

const width = Math.round(Dimensions.get("screen").width)
const height = Dimensions.get("screen").height

const ListMaterials = ({
  data,
}) => {
  const navigation=useNavigation()
  console.log(data)
  return(
    <FlatList
      data={data}
      keyExtractor={(item, index)=>`${item}`}
      renderItem={({item, index})=>{
        return(
          <TouchableOpacity 
						style={{flexDirection:'row', alignItems: 'center', margin: 5}}
						onPress={()=>navigation.navigate("PDF", {linkPdf: item})}
					>
						<MaterialIcons name="panorama-fish-eye" size={12} />
						<Text style={{color:'#3E89AE', textDecorationLine:'underline', fontSize: 18, marginLeft: 5}}>Material {index + 1}</Text>
					</TouchableOpacity>
        )
      }}
    />
)};

export default ListMaterials;
