import React, {useState, useEffect} from 'react';
import { Text, View, FlatList, Dimensions } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import {useSelector, useDispatch} from 'react-redux';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import authAPI from '../api/auth';
import STYLES from '../styles/style';

const width = Math.round(Dimensions.get("screen").width)
const height = Dimensions.get("screen").height

const AnsweredCard = ({
  question_id, remark, state_answer, student_answer_id
}) => {
  const [options, setOptions] = useState([])
  const token = useSelector(state => state.user.token)
  useEffect(()=>{
    async function getOptions(){
			try{
        const response = await authAPI.get(`/questions/one/${question_id}`, {'headers': { 'Authorization': `Bearer ${token}`}})
				setOptions(response.data.option)
			} catch(e){
				console.log(e)
			}
		}
		getOptions()
  },[])

  return (
    <View style={{margin: width * 0.01}}>
      <FlatList
        data={options}
        renderItem={({item})=>{
          return(
            <View
              style={{flexDirection:'row', marginVertical: width * 0.01, alignItems:'center',}}
            >
              {item.answer ? <MaterialIcons name="check" size={15} style={{color:"#61D3A4"}}/>:
              student_answer_id == item.id ? <MaterialIcons name="radio-button-checked" size={15} style={{color:"#3E89AE"}}/>:
              <MaterialIcons name="radio-button-unchecked" size={15} style={{color:"#000"}}/>
              }
              <Text style={{ marginLeft: width * 0.02, fontSize: height * 0.017, fontWeight: item.answer || student_answer_id == item.id ? 'bold': null}}>{item.option}</Text>
            </View>
          )
        }}
      />
      {state_answer == false &&
        <Text style={{color:"#999", fontWeight:'bold', marginTop: height * 0.02}}>Remark</Text>
      }
      {state_answer == false &&
        <Text>{remark}</Text>
      }
    </View>
)};

export default AnsweredCard;
