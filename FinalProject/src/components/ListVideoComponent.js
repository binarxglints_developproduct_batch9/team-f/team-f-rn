import React from 'react';
import { Text, View, Pressable, Dimensions, FlatList } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { useNavigation } from '@react-navigation/native';

const width = Math.round(Dimensions.get("screen").width)
const height = Dimensions.get("screen").height

const ListVideoComponent = ({
  data, id, title, poster
}) => {
  const navigation = useNavigation()
  return(
    <FlatList 
      data={data}
      keyExtractor={(item)=> item._id}
      renderItem={({item, index})=>{
        return(
          <Pressable
            style={({ pressed }) => [
              {
                backgroundColor: pressed
                  ? 'rgb(210, 230, 255)'
                  : !item.isPermission ? '#F5F5F5' : '#fff'
              }, {flexDirection:'row', borderColor:'rgba(229,229,229,1)', marginBottom: 1, borderBottomWidth: 1, padding: width * 0.05, alignItems:'center',}
            ]}
            onPressOut={()=>{
              navigation.navigate('CoursePlay', {id: id, index: index, title:title, poster:poster})
            }}
            disabled={item.isPermission ? false : true}
          >
            <MaterialIcons name={item.isPermission ? "play-arrow" : 'lock'} size={24}/>
            <Text>{item.lesson.title}</Text>
          </Pressable>
        )
      }}
    />
)};

export default ListVideoComponent;
